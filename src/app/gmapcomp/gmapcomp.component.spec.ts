import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GmapcompComponent } from './gmapcomp.component';

describe('GmapcompComponent', () => {
  let component: GmapcompComponent;
  let fixture: ComponentFixture<GmapcompComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GmapcompComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GmapcompComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
