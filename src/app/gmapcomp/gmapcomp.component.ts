import { Component, OnInit, ElementRef, ViewChild, NgZone,Input,ChangeDetectorRef } from '@angular/core';
import { FormControl } from '@angular/forms';
import { } from 'googlemaps';
import { MapsAPILoader } from '@agm/core';
import { Placeholder } from '@angular/compiler/src/i18n/i18n_ast';
import {GeoService} from './../geo.service';

@Component({
  selector: 'app-gmapcomp',
  templateUrl: './gmapcomp.component.html',
  styleUrls: ['./gmapcomp.component.css']
})
export class GmapcompComponent implements OnInit {

  title: string = 'AGM project';
  latitude: number;
  longitude: number;
  zoom:number;
  showhidemap:boolean = false;
  error:any;
  public searchControl: FormControl;
  @Input() placeholder:any="search for location";
  @ViewChild("search")
  public searchElementRef: ElementRef;
  
  //newly added
  behavior:any;

  public query: any;
  public position: string;
  public locations: Array<any>;
  

  constructor(private mapsAPILoader: MapsAPILoader,private ngZone: NgZone,private geoservice:GeoService){
    this.query = "";
    this.position = "37.7397,-121.4252";
  }
  ngOnInit() {
    //create search FormControl
    this.searchControl = new FormControl();
    this.setCurrentLocation();
    //load Places Autocomplete
    this.mapsAPILoader.load().then(() => {
      let geocoder = new google.maps.Geocoder();
      this.geoservice.initGeocoder();
      this.getAddress();
      let autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, {
        types: ["address"]
      });
      autocomplete.addListener("place_changed", () => {
        this.ngZone.run(() => {
          //get the place result
          let place: google.maps.places.PlaceResult = autocomplete.getPlace();

          //verify result
          if (place.geometry === undefined || place.geometry === null) {
            return;
          }

          //set latitude, longitude and zoom
          this.latitude = place.geometry.location.lat();
          this.longitude = place.geometry.location.lng();
          this.zoom = 12;
        });
      });
    });
    //this.behavior = new H.mapevents.Behavior(new H.mapevents.MapEvents(map));
    
  }

  // Get Current Location Coordinates
  private setCurrentLocation() {
    if ('geolocation' in navigator) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.latitude = position.coords.latitude;
        this.longitude = position.coords.longitude;
        this.zoom = 15;
      });
    }
  }

  public placeMarker(event){
    this.latitude = event.coords.lat;
    this.longitude = event.coords.lng;
    this.getAddressFromLatLng();
  }

  public getAddress() {
    if(this.query != "") {
        this.geoservice.getAddress(this.query).subscribe(result => {
            console.log(result);
            this.latitude = result.lat;
            this.longitude = result.lng;
        }, error => {
            console.error(error);
            this.error = error;
        });
    }
  }
  public getAddressFromLatLng() {
    if(this.latitude != 0 &&this.longitude!=0) {
        this.geoservice.getAddressFromLatLng({lat:this.latitude,lng:this.longitude}).subscribe(result => {
            console.log(result);
            this.query = '';
        }, error => {
            console.error(error);
            this.error = error;
        });
    }
  }
}
