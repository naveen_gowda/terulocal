import { Component, OnInit, } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
import {CartItem,ItemError} from './../model';
import {FormControl, FormGroupDirective, NgForm, Validators} from '@angular/forms';

import $ from 'jquery';
@Component({
  selector: 'app-placeorder',
  templateUrl: './placeorder.component.html',
  styleUrls: ['./placeorder.component.css']
})
export class PlaceorderComponent implements OnInit {
  pagetype:any;//type of page delivery==>1,preorder==>2,schedule==>3,daily service==>4
  items: string[] = ['Boots', 'Clogs', 'Loafers', 'Moccasins', 'Sneakers'];
  enableAdd:boolean=false;
  cartDetails:any[]=[];
  newItem:string='';
  itemError:ItemError={ErrorStatus:false,ErrorMessage:''};
  billdetails:any={
    totalItem:{lable:'Total Items',value:0},
    totalCost:{lable:'Total Cost',value:0}
  }
  constructor(private activeroute: ActivatedRoute) { 
    this.pagetype = this.activeroute.snapshot.paramMap.get('id');
  }

  ngOnInit() {
  }

  public addItemToCart(event){
    if(this.newItem==''){
      this.itemError={ErrorStatus:true,ErrorMessage:'Item name should not be empty!. Please add proper name and try again'};
      $('#newitem').focus();
      return;
    }
    else{
      this.cartDetails.push({ItemID:this.cartDetails.length+"_item",ItemName:this.newItem,ItemCount:1})
      this.itemError={ErrorStatus:false,ErrorMessage:''};
      this.cartDetails
      this.billdetails.totalItem.value = this.cartDetails.length;
      this.newItem = '';
    }
  }

  public enableAddButton(flag){
    this.enableAdd = flag;
  }

  public calculateTotItemandCost(){
    this.cartDetails.forEach(()=>{
      
    });
  }

  public modifyItemCount(flag,item){
    if(flag.toLowerCase()=='plus'){
      item.ItemCount ++;
    }else{
      item.ItemCount --;
    }
  }

}
