import { BrowserModule } from '@angular/platform-browser';
import { NgModule, ApplicationRef } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { RoutingModule } from './app-routing.model';
import {MatTabsModule,MatPaginatorModule,MatButtonToggleModule,MatCardModule,MatButtonModule, MatFormFieldModule,
  MatInputModule, MatIconModule,MatSelectModule,MatListModule,MatSidenavModule,MatChipsModule} from '@angular/material'
import { AgmCoreModule } from '@agm/core';
import { InlineSVGModule } from 'ng-inline-svg';
import { AppComponent } from './app.component';
import { DeliverComponent } from './deliver/deliver.component';
import { HomeComponent } from './home/home.component';
import { PlaceorderComponent } from './placeorder/placeorder.component';
import { GmapcompComponent } from './gmapcomp/gmapcomp.component';
import { GeoService } from './geo.service'

@NgModule({
  declarations: [
    AppComponent,
    DeliverComponent,
    HomeComponent,
    PlaceorderComponent,
    GmapcompComponent
  ],
  imports: [
    BrowserModule,
    CommonModule,
    FormsModule,
    RoutingModule,
    ReactiveFormsModule,
    InlineSVGModule.forRoot({ baseUrl: window.location.origin}),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyAvMTVwiZc029jsurGdFOTuxhTu2STWI5k',
      libraries: ["places"]
    }),
    BrowserAnimationsModule,MatChipsModule,
    MatPaginatorModule,MatTabsModule,MatButtonToggleModule,MatCardModule,MatButtonModule,MatFormFieldModule,
    MatInputModule, MatIconModule,MatSelectModule,MatListModule,MatSidenavModule
  ],
  providers: [GeoService],
  bootstrap: [AppComponent]
})
export class AppModule { }
