export class Cart
{
    public  CartItem?:CartItem[];
}
export class CartItem
{
    public  ItemID:number;
    public  ItemName:string;
    public  ItemCount?:number;
}
export class ItemError
{
    public  ErrorStatus?:boolean;
    public  ErrorMessage?:string;
}

export interface Location {
    lat: number; 
    lng: number;
}