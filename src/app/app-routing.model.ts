import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PlaceorderComponent } from './placeorder/placeorder.component';
import { HomeComponent } from './home/home.component';
const routes: Routes = [
    { 
        path: '',
        component: HomeComponent
      },
      { 
        path: 'order',
        children:[
          { path: ':id', 
          component: PlaceorderComponent, 
          pathMatch: 'full'}
        ],
      },
];

@NgModule({
    imports: [RouterModule.forRoot(routes, {onSameUrlNavigation: 'reload'})],
    exports: [RouterModule],
    providers: []
})
export class RoutingModule {

}