import { Injectable } from '@angular/core';
import { MapsAPILoader } from '@agm/core';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { of } from 'rxjs/observable/of';
import { filter, catchError, tap, map, switchMap } from 'rxjs/operators';
import { fromPromise } from 'rxjs/observable/fromPromise';

declare var google: any;

@Injectable({
    providedIn: 'root'
})
export class GeoService {

    public geocoder: any;
    private observer = new Subject<any>();
    public constructor() {
    }

    public initGeocoder() {
    this.geocoder = new google.maps.Geocoder();
    }

    public getAddress(query: any):Observable<any> {
        this.geocoder.geocode({'address': query}, (results, status) => {
            if (status == google.maps.GeocoderStatus.OK) {
                this.observer.next({
                lat: results[0].geometry.location.lat(), 
                lng: results[0].geometry.location.lng()
                });
                return this.observer.asObservable();
            } else {
                //console.log('Error - ', results, ' & Status - ', status);
                this.observer.next({ lat: 0, lng: 0 ,Error:results,Status:status});
                return this.observer.asObservable();
            }
        });
        return this.observer.asObservable();
    }

    public getAddressFromLatLng(query: any):Observable<any> {
        console.log(query);
        this.geocoder.geocode({'latLng': query}, (results, status) => {
            console.log(results,status);
            if (status == google.maps.GeocoderStatus.OK) {
                this.observer.next({
                lat: results[0].geometry.location.lat(), 
                lng: results[0].geometry.location.lng()
                });
                return this.observer.asObservable();
            } else {
                //console.log('Error - ', results, ' & Status - ', status);
                this.observer.next({ lat: 0, lng: 0 ,Error:results,Status:status});
                return this.observer.asObservable();
            }
        });
        return this.observer.asObservable();
        // return new Promise((resolve, reject) => {
        //     this.geocoder.reverseGeocode({ prox: query, mode: "retrieveAddress" }, result => {
        //         if(result.Response.View.length > 0) {
        //             if(result.Response.View[0].Result.length > 0) {
        //                 resolve(result.Response.View[0].Result);
        //             } else {
        //                 reject({ message: "no results found" });
        //             }
        //         } else {
        //             reject({ message: "no results found" });
        //         }
        //     }, error => {
        //         reject(error);
        //     });
        // });
    }

}